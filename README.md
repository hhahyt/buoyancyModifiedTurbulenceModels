buoyancyModifiedTurbulenceModels
================================

## General

The buoyancy-modified turbulence models are developed to simulate offshore 
and coastal engineering processes. The buoyancy-modified turbulence 
models not only result in a stable wave propagation model without wave 
damping but they also predict the turbulence level inside the flow field 
more accurately in the surf zone.

This repository is maintained for OpenFOAM ESI OpenCFD releases only 
(currently OpenFOAM-v1812). The source code for older and other OpenFOAM 
distributions is available at: 
<https://github.com/BrechtDevolder-UGent-KULeuven/buoyancyModifiedTurbulenceModels>.

## References and citing

The buoyancy-modified turbulence models have been developed within the 
PhD thesis of Brecht Devolder at the Department of Civil Engineering at 
Ghent University and KU Leuven, funded by the Research Foundation – 
Flanders (FWO), Belgium (Ph.D. fellowship 1133817N). The PhD thesis is available 
for download at: <https://biblio.ugent.be/publication/8564551>. 

If you want to reference the model in your publications, you can use the 
following references in which the implementation and validation details 
are published:  
- Devolder, B., Rauwoens, P., & Troch, P. (2017). Application of a buoyancy-modified *k-ω SST* turbulence model to simulate wave run-up around a monopile subjected to regular waves using OpenFOAM<sup>®</sup>. Coastal Engineering, 125, 81–94. [doi:10.1016/j.coastaleng.2017.04.004](https://doi.org/10.1016/j.coastaleng.2017.04.004).
- Devolder, B., Troch, P., & Rauwoens, P. (2018). Performance of a buoyancy-modified *k-ω* and *k-ω SST* turbulence model for simulating wave breaking under regular waves using OpenFOAM<sup>®</sup>. Coastal Engineering, 138, 49–65. [doi:10.1016/j.coastaleng.2018.04.011](https://doi.org/10.1016/j.coastaleng.2018.04.011).
- Devolder, B. (2018). Hydrodynamic Modelling of Wave Energy Converter Arrays. PhD thesis. Ghent University, Faculty of Engineering and Architecture, Ghent, Belgium. KU Leuven, Faculty of Engineering Technology, Bruges, Belgium.

## Installation for OpenFOAM ESI OpenCFD releases

- Open a linux terminal and download the package using git:

      git clone https://gitlab.com/brecht.devolder/buoyancyModifiedTurbulenceModels.git
      cd buoyancyModifiedTurbulenceModels

- Source the OpenFOAM environment: 

      source $HOME/OpenFOAM/OpenFOAM-vYYMM/etc/bashrc        
      
- Compile the source code to build a shared library:

      wmake libso

## How to use

- Include the buoyancyModifiedTurbulenceModels library in system/controlDict:

      libs
      (
          "libbuoyancyModifiedTurbulenceModels.so"
      );
		
- Add the correct turbulence model in constant/turbulenceProperties:

      simulationType  RAS;

      RAS
      {
          RASModel        kOmegaSSTBuoyancy; //kOmegaBuoyancy;
          turbulence      on;
          printCoeffs     on;
      }

- Modify system/fvSchemes:

      ddt(k)
      ddt(omega)
      ...
      div(phi,k)
      div(phi,omega)
		
    to

      ddt(rho,k)
      ddt(rho,omega)
      ...
      div((interpolate(rho)*phi),k)
      div((interpolate(rho)*phi),omega)

## Tutorials

*coming soon...*


## Contact

Brecht Devolder (PhD, MSc)  
Department of Civil Engineering - Construction TC  
KU Leuven - Campus Bruges  
Spoorwegstraat 12, B-8200 Brugge, Belgium  
<brecht.devolder@kuleuven.be>  
